<?php

namespace Crms\Hosting;

use config;
use Core\Crypt\OpenSSLCrypt;
use Core\Model\Hosting;
use Core\Model\HostingSetting;
use Exception;

/**
 * Class SftpHostingActivity
 */
class SftpHostingActivity implements HostingActivityInterface {
	private const TIMEOUT = 10;

	/**
	 * @var Hosting
	 */
	private $hosting;

	/**
	 * @var HostingSetting
	 */
	private $hostingSettings;

	/**
	 * SftpHostingActivity constructor.
	 *
	 * @param Hosting $hosting
	 * @param HostingSetting $hostingSettings
	 */
	public function __construct(Hosting $hosting, HostingSetting $hostingSettings) {
		$this->hosting = $hosting;
		$this->hostingSettings = $hostingSettings;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function isActive(): bool {
		if (!$this->canConnectToSocket()) {
			throw new Exception('Network resource is not available.');
		}

		$connection = ssh2_connect(
			$this->hosting->getHost(),
			$this->hostingSettings->getPublishPort()
		);

		if (!$connection) {
			throw new Exception('Unable to establish connection to SSH server.');
		}

		$crypt = new OpenSSLCrypt(config::get('crypt.key'));
		$auth = @ssh2_auth_password(
			$connection,
			$this->hosting->getUsername(),
			$crypt->decrypt($this->hosting->getEncryptedPassword())
		);

		if (!$auth) {
			unset($connection);
			throw new Exception('Unable to authenticate to SSH server.');
		}

		$sftp = ssh2_sftp($connection);
		unset($connection);

		if (!$sftp) {
			throw new Exception('Unable to get SFTP subsystem.');
		}

		return true;
	}

	/**
	 * @return bool
	 */
	private function canConnectToSocket(): bool {
		$socket = fsockopen(
			$this->hosting->getHost(),
			$this->hostingSettings->getPublishPort(),
			$errorNumber, $errorString,
			self::TIMEOUT
		);

		if ($socket) {
			fclose($socket);

			return true;
		}

		return false;
	}
}