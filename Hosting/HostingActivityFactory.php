<?php

namespace Crms\Hosting;

use Core\Model\Hosting;
use Core\Model\HostingSetting;

/**
 * Class HostingActivityFactory
 */
class HostingActivityFactory {

	private const PTP_FTP = 'ftp';
	private const PTP_SFTP = 'sftp';

	/**
	 * @param Hosting $hosting
	 * @param HostingSetting $hostingSettings
	 *
	 * @return HostingActivityInterface
	 */
	public static function create(
		Hosting $hosting,
		HostingSetting $hostingSettings
	): ?HostingActivityInterface {
		$protocol = strtolower($hostingSettings->getPublishTransferProtocol());

		if ($protocol == self::PTP_FTP) {
			return new FtpHostingActivity($hosting, $hostingSettings);
		} else if ($protocol == self::PTP_SFTP) {
			return new SftpHostingActivity($hosting, $hostingSettings);
		}

		return null;
	}
}