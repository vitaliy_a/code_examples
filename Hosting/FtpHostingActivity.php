<?php

namespace Crms\Hosting;

use config;
use Core\Crypt\OpenSSLCrypt;
use Core\Model\Hosting;
use Core\Model\HostingSetting;
use Exception;

/**
 * Class FtpHostingActivity
 */
class FtpHostingActivity implements HostingActivityInterface {
	private const TIMEOUT = 10;

	/**
	 * @var Hosting
	 */
	private $hosting;

	/**
	 * @var HostingSetting
	 */
	private $hostingSettings;

	/**
	 * FtpHostingActivity constructor.
	 *
	 * @param Hosting $hosting
	 * @param HostingSetting $hostingSettings
	 */
	public function __construct(Hosting $hosting, HostingSetting $hostingSettings) {
		$this->hosting = $hosting;
		$this->hostingSettings = $hostingSettings;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function isActive(): bool {
		$connection = ftp_connect(
			$this->hosting->getHost(),
			$this->hostingSettings->getPublishPort(),
			self::TIMEOUT
		);

		if (!$connection) {
			throw new Exception('Unable to open FTP connection.');
		}

		$crypt = new OpenSSLCrypt(config::get('crypt.key'));
		$ftp = @ftp_login(
			$connection,
			$this->hosting->getUsername(),
			$crypt->decrypt($this->hosting->getEncryptedPassword())
		);

		if (!$ftp) {
			ftp_close($connection);
			throw new Exception('Unable to login in to FTP server with given credentials.');
		}

		ftp_pasv($connection, true);
		$contents = ftp_nlist($connection, ".");
		ftp_close($connection);

		if ($contents === false) {
			throw new Exception('Unable to get list of files in current directory of FTP server.');
		}

		return true;
	}
}