<?php

namespace Crms\Hosting;

/**
 * Interface HostingActivityInterface
 */
interface HostingActivityInterface {
	/**
	 * @return bool
	 */
	public function isActive(): bool;
}
