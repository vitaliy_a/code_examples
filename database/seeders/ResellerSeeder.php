<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Reseller;
use Illuminate\Database\Seeder;

/**
 * Class ResellerSeeder
 */
class ResellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        if (!$companies->count()) {
            $companies = Company::factory(10)->create();
        }

        $companies->each(
            function (Company $company) {
                Reseller::factory(rand(0, 3))->for($company)->create();
            }
        );
    }
}
