<?php

namespace App\Http\Requests\Reseller;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ResellerCreateRequest
 */
class ResellerCreateRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'is_demo' => 'boolean',
            'company_id' => 'integer|exists:reseller_company',
        ];
    }
}
