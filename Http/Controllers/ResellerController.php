<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reseller\ResellerCreateRequest;
use App\Http\Requests\Reseller\ResellerUpdateRequest;
use App\Models\Reseller;

/**
 * Class ResellerController
 */
class ResellerController extends Controller
{
    /**
     * Store a newly created reseller in storage.
     *
     * @param ResellerCreateRequest $request
     * @param Reseller $reseller
     * @return Reseller
     */
    public function store(ResellerCreateRequest $request, Reseller $reseller)
    {
        return Reseller::create($request->validated())->refresh();
    }

    /**
     * Update the specified reseller in storage.
     *
     * @param ResellerUpdateRequest $request
     * @param Reseller $reseller
     * @return Reseller
     */
    public function update(ResellerUpdateRequest $request, Reseller $reseller)
    {
        $reseller->update($request->validated());

        return $reseller;
    }

    /**
     * Displays specified reseller
     *
     * @param Reseller $reseller
     * @return Reseller
     */
    public function show(Reseller $reseller)
    {
        return $reseller;
    }

    /**
     * Displays list of resellers
     *
     * @return mixed
     */
    public function index()
    {
        return Reseller::paginate();
    }
}
