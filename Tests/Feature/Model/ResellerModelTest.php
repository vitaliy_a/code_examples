<?php

// phpcs:disable PSR1.Methods.CamelCapsMethodName

namespace Tests\Feature\Model;

use App\Models\Company;
use App\Models\Reseller;
use Tests\TestCase;

/**
 * Class ResellerModelTest
 */
class ResellerModelTest extends TestCase
{
    /**
     * @test
     */
    public function reseller_created_with_active_status()
    {
        $reseller = Reseller::factory()->create();

        $this->assertEquals(Reseller::STATUS_ACTIVE, $reseller->status);
    }

    /**
     * @test
     */
    public function reseller_created_with_demo_flag_in_false_by_default()
    {
        $reseller = Reseller::factory()->create([
            'is_demo' => null
        ]);

        $this->assertEquals(false, $reseller->is_demo);
    }

    /**
     * @test
     */
    public function reseller_created_with_default_domain()
    {
        $reseller = Reseller::factory()->create();

        $this->assertEquals(
            config('siteplus.reseller.domain'),
            $reseller->domain
        );
    }

    /**
     * @test
     */
    public function reseller_created_with_default_dynamic_subdomain()
    {
        $reseller = Reseller::factory()->create();

        $this->assertEquals(
            config('siteplus.reseller.dynamic_subdomain'),
            $reseller->dynamic_subdomain
        );
    }

    /**
     * @test
     */
    public function company_can_be_assigned_to_the_reseller()
    {
        $company = Company::factory()->create();

        $reseller = Reseller::factory()->create([
            'company_id' => $company->company_id,
        ]);

        $this->assertEquals(
            $reseller->company->company_id,
            $company->company_id
        );
    }
}
