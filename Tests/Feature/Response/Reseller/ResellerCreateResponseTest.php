<?php

namespace Tests\Feature\Response\Reseller;

// phpcs:disable PSR1.Methods.CamelCapsMethodName

use App\Models\Reseller;
use Tests\TestCase;

/**
 * Class ResellerCreateResponseTest
 */
class ResellerCreateResponseTest extends TestCase
{
    /**
     * @test
     */
    public function create_endpoint_has_reseller_entity_in_response()
    {
        $reseller = Reseller::factory()->raw();

        $response = $this->postJson(
            route('resellers.store'),
            $reseller
        );

        $response->assertSuccessful()
            ->assertJson($reseller);
    }

    /**
     * @test
     */
    public function create_response_contains_required_fields()
    {
        $reseller = Reseller::factory()->raw();

        $response = $this->postJson(
            route('resellers.store'),
            $reseller
        );

        $response->assertSuccessful()
            ->assertJsonStructure(
                [
                    'reseller_id',
                    'name',
                    'status',
                    'created_at',
                    'updated_at',
                    'company_id',
                ]
            );
    }
}
