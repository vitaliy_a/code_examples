<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Reseller
 */
class Reseller extends Model
{
    use HasFactory;

    public const STATUS_ACTIVE = 'active';
    public const STATUS_TERMINATED = 'terminated';

    public const CREATED_AT = 'date_added';
    public const UPDATED_AT = 'last_update';

    protected $connection = 'mysql_resellers';
    protected $table = 'reseller';
    protected $primaryKey = 'reseller_id';

    protected $fillable = [
        'name',
        'is_demo',
        'status',
        'company_id',
    ];

    protected $attributes = [
        'status' => self::STATUS_ACTIVE,
        'is_demo' => false,
    ];

    protected $visible = [
        'reseller_id',
        'name',
        'status',
        'is_demo',
        'created_at',
        'updated_at',
        'company_id',
    ];

    protected $appends = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return mixed
     */
    public function getCreatedAtAttribute()
    {
        return $this->date_added;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAtAttribute()
    {
        return $this->last_update;
    }

    /**
     * The "booted" method of the model.
     * Set default values for hidden fields in reseller table.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (self $reseller) {
            $reseller->domain = config('siteplus.reseller.domain');
            $reseller->dynamic_subdomain = config('siteplus.reseller.dynamic_subdomain');
        });
    }

    /**
     * Get the company associated with the reseller.
     *
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
